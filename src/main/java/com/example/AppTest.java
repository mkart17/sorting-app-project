package com.example;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class AppTest {

    private final String[] input;
    private final int[] expected;

    public AppTest(String[] input, int[] expected) {
        this.input = input;
        this.expected = expected;

    }
    @Parameters
    public static Collection <Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new String[]{"3", "5", "1"}, new int[]{1, 3, 5}},
                {new String[]{"2", "0", "7"}, new int[]{0, 2, 7}},
                {new String[]{"3", "2", "4", "6", "19", "3", "87", "8", "9", "2", "43"}, new int[]{2, 2, 3, 3, 4, 6, 8, 9, 19, 43, 87}},
                {new String[]{}, new int[]{}},
                {new String[]{"5"}, new int[]{5}},


        });
    }
    @Test
    public void TestApp() {
        App.main(input);
        int[] actual = Arrays.stream(input).mapToInt(Integer::parseInt).toArray();
        Arrays.sort(actual);
        Assert.assertArrayEquals(expected, actual);

    }
}
